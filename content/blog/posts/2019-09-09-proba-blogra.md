---
title: Proba blogra
description: semmi kulonons majd tortenik valami vagy ki tudja.
date: '2019-09-09T10:57:50+02:00'
---
## valami ilyesmi
```
param(
    [string]
    $Version = "2.2",
    [string]
    $Name,
    [string]
    $Destination
)

function Visitor {
    param (
        [System.Object]
        $File,
        [string]
        $Path
    )
    $ActualPath = "$Path\$File"
    if ($File.csprojType) {
        dotnet new $File.csprojType -f "netcoreapp$Version" -o "$Path\$Name$($File.name)"
    }
    foreach($Element in $File){
        Visitor -File $Element -Path $ActualPath
    }
}

if (!$Name) {
    Write-Host("-Name option is required!")
    exit
}
if (!$Destination) {
    $Base = Get-Location
    exit
}else {
    # TODO \ jel a végén
    $Base = $Destination
}

$DotNetCore="netcoreapp$Version"
$WorkspaceRoot="$base\$Name"


$Structure = Get-Content -Raw -Path structure.json | ConvertFrom-Json

foreach($Item in $Structure.structure){
    Visitor -File $Item -Path $WorkspaceRoot
}


# New-Item -Path "$WorkspaceRoot\src\$Name.DataAccess" -ItemType directory
# New-Item -Path "$WorkspaceRoot\src\$Name.Logic" -ItemType directory
# New-Item -Path "$WorkspaceRoot\src\$Name.WebApi" -ItemType directory
# New-Item -Path "$WorkspaceRoot\test\$Name.Tests" -ItemType directory

# dotnet new classlib -f $DotNetCore -o "$WorkspaceRoot\src\$Name.DataAccess"
# dotnet new classlib -f $DotNetCore -o "$WorkspaceRoot\src\$Name.Logic"
# dotnet new webapi -f $DotNetCore -o "$WorkspaceRoot\src\$Name.WebApi"
# dotnet new xunit -f $DotNetCore -o "$WorkspaceRoot\test\$Name.Tests"

# dotnet new sln -o $WorkspaceRoot
# dotnet sln "$WorkspaceRoot\$Name.sln" add "$WorkspaceRoot\src\$Name.DataAccess\$Name.DataAccess.csproj"
# dotnet sln "$WorkspaceRoot\$Name.sln" add "$WorkspaceRoot\src\$Name.Logic\$Name.Logic.csproj"
# dotnet sln "$WorkspaceRoot\$Name.sln" add "$WorkspaceRoot\src\$Name.WebApi\$Name.WebApi.csproj"
# dotnet sln "$WorkspaceRoot\$Name.sln" add "$WorkspaceRoot\test\$Name.Tests\$Name.Tests.csproj"


# dotnet add "$WorkspaceRoot\src\$Name.Logic\$Name.Logic.csproj" reference "$WorkspaceRoot\src\$Name.DataAccess\$Name.DataAccess.csproj"
# dotnet add "$WorkspaceRoot\src\$Name.WebApi\$Name.WebApi.csproj" reference "$WorkspaceRoot\src\$Name.Logic\$Name.Logic.csproj"
# dotnet add "$WorkspaceRoot\test\$Name.Tests\$Name.Tests.csproj" reference "$WorkspaceRoot\src\$Name.DataAccess\$Name.DataAccess.csproj"
# dotnet add "$WorkspaceRoot\test\$Name.Tests\$Name.Tests.csproj" reference "$WorkspaceRoot\src\$Name.Logic\$Name.Logic.csproj"

# dotnet add "$WorkspaceRoot\src\$Name.WebApi\$Name.WebApi.csproj" package Newtonsoft.Json --version 12.0.2
# dotnet add "$WorkspaceRoot\test\$Name.Tests\$Name.Tests.csproj" package Moq --version 4.13.0


# param ([string] $DBConnectionString)

# Import-Module "..\..\Src\OKF.Shared\bin\Debug\netcoreapp2.2\OKF.Shared.dll"

# <#
# .SYNOPSIS
# Bejárja a szerepköröket.

# .DESCRIPTION
# A paraméterül kapot szerepkörtől kezdve pre-order módon bejárja a szerepkörök közötti hierarchia fát.

# .PARAMETER role
# A szerepkör amit fel kívánunk dolgozni.

# .PARAMETER parentId
# A feldolgozni kívánt szerepkör szülejének az id-ja.

# .EXAMPLE
# Visitor -role $role -parentId $parentId
# #>
# function Visitor {
#     param ([System.Object] $role, [Guid] $parentId)

#     $roleId = [Guid]::newGuid()
#     CreateDBRole -roleId $roleId -rolename $role.role_name -parentId $parentId -roleType $role.type

#     AllowAction -role $role

#     # CreateKRAPIRole -roleName $role.role_name

#     foreach($element in $role.subroles){
#         Visitor -role $element -parentId $roleId
#     }
# }

# <#
# .SYNOPSIS
# Szerepkör adatbázisba mentése.

# .DESCRIPTION
# Végrehajtja egy szerepkör beszúrását a organization_role adatbázistáblába, figyelembe véve a szerepkörök közötti hierarchiát.

# .PARAMETER roleId
# A menteni kívánt szerepkör id-ja.

# .PARAMETER roleName
# A menteni kívánt szerepkör kódja (neve).

# .PARAMETER parentId
# A menteni kívánt szerepkör szülejének az id-ja.

# .PARAMETER roleType
# A menteni kívánt szerepkör típusa.

# .EXAMPLE
# CreateDBRole -roleId $roleId -rolename $roleName -parentId $parentId -roleType $roleType
# #>
# function CreateDBRole {
#     param ([Guid] $roleId,[String] $roleName, [Guid] $parentId, [string] $roleType)
    
#     $roleTypeId = [OKF.Shared.Enums.OrganizationRoleType]::$roleType -as [int]

#     $Datatable = New-Object System.Data.DataTable
#     $DBConn.Open()
#     $DBCmd = $DBConn.CreateCommand()
#     $DBCmd.CommandText = "SELECT * FROM organization_role WHERE code = '$roleName';"
#     $Reader = $DBCmd.ExecuteReader()
#     $Datatable.Load($Reader)
#     $DBConn.Close()

#     if ($Datatable.Rows.Count -eq 0) {
#         $DBConn.Open()
#         $DBCmd = $DBConn.CreateCommand()
#         if($parentId -eq [Guid]::Empty){
#             $DBCmd.CommandText = "INSERT INTO organization_role (id, code, organization_role_type) values ('$roleId', '$roleName', $roleTypeId);"
#         }else{
#             $DBCmd.CommandText = "INSERT INTO organization_role (id, code, organization_role_type, parent_id) values ('$roleId', '$roleName', $roleTypeId, '$parentId');"
#         }
#         $DBCmd.CommandText
#         $DBCmd.ExecuteReader()
#         $DBConn.Close()
#     }
# }

# <#
# .SYNOPSIS
# Műveletek (Action-ök) adatbázisba mentése.

# .DESCRIPTION
# Végrehajtja a műveletek (action-ök) beszúrását a role_action adatbázistáblába.

# .PARAMETER role
# Az a szerepkör amelyhez műveleteket kívánunk hozzákapcsolni.

# .EXAMPLE
# AllowAction -role $role

# .NOTES
# A paraméterül kapott szerepkör műveletein végig iterálva megfuttatja a szükséges insert scripteket.
# #>
# function AllowAction {
#     param ([System.Object] $role)
    
#     foreach($action in $role.actions){

#         $actionId = [OKF.Shared.Enums.ApplicationRight]::$action -as [int]

#         $Datatable = New-Object System.Data.DataTable
#         $DBConn.Open()
#         $DBCmd = $DBConn.CreateCommand()
#         $DBCmd.CommandText = "SELECT * FROM role_action WHERE application_right = $actionId AND role_id = (SELECT id FROM organization_role WHERE code = '$($role.role_name)');"
#         $Reader = $DBCmd.ExecuteReader()
#         $Datatable.Load($Reader)
#         $DBConn.Close()

#         if ($Datatable.Rows.Count -eq 0) {
#             $Guid = [Guid]::newGuid()
#             $DBConn.Open()
#             $DBCmd = $DBConn.CreateCommand()
#             $DBCmd.CommandText = "INSERT INTO role_action (id, application_right, role_id) VALUES ('$Guid', $actionId, (SELECT id FROM organization_role WHERE code = '$($role.role_name)'));"
#             Write-Output " -- $action -- $($DBCmd.CommandText)"
#             $DBCmd.ExecuteReader()
#             $DBConn.Close()
#         }
#     }
# }

# <#
# .SYNOPSIS
# Feltölti a szerepkört a KRAPI:JogosultságKezelő api-ba.

# .DESCRIPTION
# A KRAPI:JogosultságKezelő api megfelelő endpoint-ját meghívva feltölti a szerepkört.

# .PARAMETER roleName
# A KRAPI:JogosultságKezelőbe feltölteni kívánt szerepkör kódja (neve).

# .EXAMPLE
# CreateKRAPIRole -roleName $roleName

# .NOTES
# Egyelőre nem található ilyen endpoint a KRAPI:JogosultságKezelőben.
# #>
# function CreateKRAPIRole {
#     param([String] $roleName)

#     # $Response = Invoke-RestMethod -URI http://192.168.200.147:8082/jogkezelo/szerepkorok
# }

# $MyServer = "192.168.200.147"
# $MyPort  = "5433"
# $MyDB = "okf"
# $MyUid = "postgres"
# $MyPass = "ns"

# $DBConnectionString = "Driver={PostgreSQL UNICODE(x64)};Server=$MyServer;Port=$MyPort;Database=$MyDB;Uid=$MyUid;Pwd=$MyPass;"
# $DBConn = New-Object System.Data.Odbc.OdbcConnection
# $DBConn.ConnectionString = $DBConnectionString
```
